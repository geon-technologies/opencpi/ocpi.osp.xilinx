#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Joel T. Palmer
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: Assigns the particular Vivado Board Part number and the RCC 
#              platform that it can target
#==============================================================================

HdlPart_zcu102=xczu9eg-2-ffvb1156e
HdlRccPlatform_zcu102=xilinx19_2_aarch64
