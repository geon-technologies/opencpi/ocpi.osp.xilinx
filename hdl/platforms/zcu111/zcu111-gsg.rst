.. zcu111 Getting Started Guide Documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _zcu111-gsg:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:

OpenCPI Xilinx ZCU111 Getting Started Guide
===========================================

.. This is the main file for the ZCU111 Getting Started Guide.
   This file contains information that is specific to the ZCU111.

.. The directives below include the files in hdl/platforms/include/ into this document.
   These files contain information that applies to all ZCU platforms. They use substitution
   strings to indicate where platform-specific values should be used when the file is included
   into a document for a specific ZCU platform. The substitution strings defined in this file
   match the strings used in the common include files. In this file, they are set to the
   values for the ZCU111 and are applied when the generic files are used in this document.


Document Revision History
-------------------------

.. csv-table:: OpenCPI ZCU111 Getting Started Guide: Revision History
   :header: "OpenCPI Version", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v2.1", "Initial Release", "7/2021"
   "v2.4", "Revise for integration with OpenCPI v2.4", "7/2022"

.. ocpi_documentation_include:: ../include/zcu_overview.inc
				
   |device_name|: ZCU111
   |platform_name|: zcu111
   |path_to_figures|: ../include/zcu111_figures/
   |device_type|: Radio Frequency System-on-Chip (RFSoC)
   |product_brief|: product brief at https://www.xilinx.com/products/boards-and-kits/zcu111.html

.. ocpi_documentation_include:: ../include/zcu_hw_setup.inc

   |device_name|: ZCU111
   |platform_name|: zcu111
   |path_to_figures|: ../include/zcu111_figures/
   |sd_boot_pins|: SW6[4:1]
   |user_guide|: section "RFSoC Device Configuration" in the ZCU111 User Guide at https://www.xilinx.com/support/documents/boards_and_kits/zcu111/ug1271-zcu111-eval-bd.pdf

.. ocpi_documentation_include:: ../include/zcu_dc_setup.inc

   |device_name|: ZCU111
   |platform_name|: zcu111
   |path_to_figures|: ../include/zcu111_figures/
   |dc_slot_config|: one FPGA Mezzanine Card Plus (FMC+) slot
   
.. ocpi_documentation_include:: ../include/zcu_sfw_setup.inc
				
   |device_name|: ZCU111
   |platform_name|: zcu111
   |prompt_name|: zcu111
   |path_to_figures|: ../include/zcu111_figures/
   |dtype_abbrv|: RFSoC
   |sd_slot_location|: next to the power switch and the micro-USB serial port
   |uart_location|: on the top side
   |uart_label|: **USB JTAG UART/J83**
