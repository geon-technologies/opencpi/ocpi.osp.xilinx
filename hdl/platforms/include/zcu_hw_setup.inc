.. Common text for ZCUxxx hardware setup.

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _|platform_name|-hw-setup:

Setting up the |device_name| Hardware
-------------------------------------

This section describes how to set up the |device_name| hardware for OpenCPI.

.. note::

   Be sure to perform these steps *before* applying power to the |device_name|.

Configuring the |device_name| to Boot from an SD Card
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default, the boot mode switch on the |device_name| is
set to boot from the Quad Serial Peripheral Interface (QSPI).
Set this switch (|sd_boot_pins|) to the positions shown in :numref:`|platform_name|-boot-switch-diagram`
so that the |device_name| will boot from the SD card. See the |user_guide| for details.

.. _|platform_name|-boot-switch-diagram:

.. figure:: |path_to_figures|boot_jmp.jpg
   :alt: 
   :align: center

   Xilinx |device_name|: Top View with SD Card Boot Mode Switch Settings

