# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/1c886af5...v2.4.2) (2022-05-26)

Initial release of zcu102 OSPs

### Enhancements
- n/a

### Bug Fixes
- n/a

### Miscellaneous
- n/a

