#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Joel Palmer
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: Assigns the particular Vivado Board Part number and the RCC 
#              platform that it can target
#==============================================================================

HdlPart_zcu111=xczu28dr-2-ffvg1517e
HdlRccPlatform_zcu111=xilinx19_2_aarch64
