#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Joel T. Palmer
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
#==============================================================================

$(if $(realpath $(OCPI_CDK_DIR)),,\
  $(error The OCPI_CDK_DIR environment variable is not set correctly.))
# This is the Makefile for the ocpi.osp.xilinx project.
include $(OCPI_CDK_DIR)/include/project.mk
