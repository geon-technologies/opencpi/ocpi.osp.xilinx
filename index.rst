.. xilinx top level project documentation
   
.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _xilinx-top-level:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:


Xilinx OSP
==========

OpenCPI System Support Package (OSP) that contains assets that support the
Xilinx\ |reg| Zynq\ |reg| UltraScale+\ |trade| processor family.

Supported systems and their associated OpenCPI HDL platform names include:

* Xilinx Zynq UltraScale+ Multiprocessor System-on-Chip (MPSoC) ZCU102 (``zcu102``)

* Xilinx Zynq UltraScale+ Radio Frequency System-on-Chip (RFSoC) ZCU111 (``zcu111``) (available in a future release)

The OSP provides a user "getting started guide" for each system to be used when
setting up the system hardware for OpenCPI and when setting up the system
for OpenCPI using the *OpenCPI Installation Guide*.

The OSP also provides a case study guide for each system
that describes the OSP development and test process followed for the system.

.. toctree::
   :maxdepth: 2
   :glob:

   hdl/platforms/gsg

..   components/components
..   hdl/primitives/primitives
..   specs/specs
